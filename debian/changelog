openntpd (1:6.2p3-3) experimental; urgency=medium

  * Drop pidfile implementation on initscript. This is only used on sysv, since
    when present, systemd service unit is preferred.
    Using a pidfile parabolically hits a bug on apparmor, since its not
    declared, hence not covered. Thanks AdamSikora! (Closes: #901589).

 -- Ulises Vitulli <dererk@debian.org>  Tue, 24 Jul 2018 12:10:27 -0300

openntpd (1:6.2p3-2) unstable; urgency=medium

  * Fix apparmor profile denying logging capability, thanks StefanoRivera!
    (Closes: #904040).
  * Upgraded debhelper compat to 11.
    - Replace dh_systemd* in favour of dh_installsystemd.
    - Remove unrequired autotools-dev dependency.
  * Bump up Standards-Version to 4.1.5.0. No changes required.
  * Update Vcs-* control fields for move to salsa.debian.org.
  * Update some missallined copyright snippets.

 -- Ulises Vitulli <dererk@debian.org>  Wed, 18 Jul 2018 19:52:43 -0300

openntpd (1:6.2p3-1) unstable; urgency=medium

  * New upstream release.
  * Fix removing updated driftfile path on debscripts.
  * Fix some dpkg-maintainer-helper missing calls on debscripts.
  * Refreshed local patches and updated manpages.
  * Update Standards-Version, refreshed obsolete build-deps.
  * Refreshed apparmor-profile due to missing execution property.

 -- Ulises Vitulli <dererk@debian.org>  Tue, 31 Oct 2017 21:44:20 -0300

openntpd (1:6.0p1-4) unstable; urgency=medium

  * Re-enable apparmor profile, Break on older apparmor-profiles-extra version.
  * Upgrade to proper copyright label.
  * Add Documentation key systemd service.
  * Conflict systemd unit against chrony, thanks VincentBlut (Closes: #849155).
  * Update VCS-* fields to reflect git.debian.org changes.
  * Update Standards-Version to 4.0.1. No changes required.

 -- Ulises Vitulli <dererk@debian.org>  Sun, 06 Aug 2017 23:41:12 -0300

openntpd (1:6.0p1-3) unstable; urgency=medium

  * Add CAP_SYS_NICE capability to systemd service unit (Closes: #855917).
    Thanks PatrikLundin!

 -- Ulises Vitulli <dererk@debian.org>  Fri, 17 Mar 2017 16:18:00 -0300

openntpd (1:6.0p1-2) unstable; urgency=medium

  * Properly apply patch for compiling at kFreeBSD-*.
  * Implement some systemd sandboxing and hardening features (Closes: #816456)
    Thanks NicolasBraud-Santoni.
  * Reload openntpd once network changes are triggered even on standalone
    (Closes: #736515).
  * Debianize manpages, thanks AntoineBeaupré (Closes: #791571, #825194).
  * Explicity notice constraint certificate verification turned off, due to
    missing libtls provided by libreSSL (not yet adopted at Debian)
    (Closes: #791534, #844069).

 -- Ulises Vitulli <dererk@debian.org>  Fri, 11 Nov 2016 18:47:56 -0300

openntpd (1:6.0p1-1) unstable; urgency=medium

  * New upstream release, fixes several issues:
   - Set MOD_MAXERROR to avoid unsynced time status when using ntp_adjtime.
   - Fixed high CPU usage when the network is down.
   - Fixed various memory leaks.
   - Switched to RMS for jitter calculations.
  * Refreshed local patches against new upstream release.
  * Minor updates to pkg policy.

 -- Ulises Vitulli <dererk@debian.org>  Fri, 11 Nov 2016 11:21:20 -0300

openntpd (1:5.7p4-4) unstable; urgency=medium

  * Welcome to the systemd party! (Closes: #816453):
    Thanks NicolasBraud-Santoni!
    - Properly monitor ntpd process.
    - Reload command is now provided.
    - Conf is now validated before (re)starting or reloading the service.
    - Further ntpd process confinement using systemd Private*, Protected* and
      CapabilityBoundingSet functionalities.

 -- Ulises Vitulli <dererk@debian.org>  Fri, 04 Mar 2016 10:22:30 -0300

openntpd (1:5.7p4-3) unstable; urgency=medium

  * Add support for GNU/kFreeBSD arc4random (Closes: 815302).

 -- Ulises Vitulli <dererk@debian.org>  Mon, 22 Feb 2016 09:39:53 -0300

openntpd (1:5.7p4-2) unstable; urgency=medium

  * Merged from experimental to unstable, 'some-water-under-the-bridge' dupload.
  * Temporary remove apparmor profile, avoid colision with ntp's at
    apparmor-profiles-extras (Closes: #769146, for now).
  * Replace and declare conflict on systemd-timesyncd.service.
    Thanks VincentBlut (Closes: #798388).

 -- Ulises Vitulli <dererk@debian.org>  Wed, 13 Jan 2016 11:09:13 -0300

openntpd (1:5.7p4-1) experimental; urgency=medium

  * The 'Uh!-So-Close!' dupload.
  * New upstream release:
    - Refresh local patch 01-use-debian-ntp-pool.patch.
    - Refresh local patch 02-syslog.patch.
  * Drop chrony from conflicts and replaces fields since its provided by
    time-daemon virtual package (Closes: #781777).
  * Remove command path on postrm script, dpkg ensures that the PATH is set to
    a reasonable value.

 -- Ulises Vitulli <dererk@debian.org>  Fri, 22 May 2015 15:24:48 -0300

openntpd (1:5.7p3-1) unstable; urgency=medium

  * New upstream postable branch release (Closes: #774865, #775116):
    - Fixed issue resolving hostnames when the network is initially unavailable
      (Closes: #775953).
    - Fixed process name logging on Linux.
    - 02-syslog.patch: refresh local patch (upstream partially adopted).
    - 03-openntpd-signals-fix.patch: drop local patch (upstrem adopted).
  * Now in sync with new portable branch maintainer, step up on 1: epoch and
    migrate from experimental to unstable.

 -- Ulises Vitulli <dererk@debian.org>  Mon, 02 Mar 2015 11:14:04 -0300

openntpd (20150108~5.7p1-1) experimental; urgency=medium

  * The 'Make-space-on-the-shelf-for-my-new-hero!' dupload.
  * New OpenNTPd portable branch maintainer, by BrentCook, import 5.7p:
    - Drop local patch 02-warn-unsupp-servers.patch (adopted upstream).
    - Drop local patch 03-manpage-no-server-by-default.patch (adopted upstream).
    - Drop local patch 05-fix-kfreebsd-ftbfs.patch (deprecated).
    - Drop local patch 06-pid.patch (adopted upstream).
    - Drop local patch 07-dnstimeout.patch (adopted upstream).
    - Drop local patch 08-openntpd-signals-fix.patch (adopted upstream).
    - Refresh local patch 02-syslog.patch.
  * Revert #701204 since its now created on service startup by ntpd.
  * Introduced ntpctl: new daemon statistics service (Closes: #775304).
  * Introduced RuntimeDirectory on systemd.service for handling /run.
  * Updated apparmor profile to reflect behaviour changes.
  * Updated README.source documentation on how to build upstream's source.
  * Remove strip flag, performed on upstream scripts.
  * Update ntpd.drift file path and apparmor's policy.

 -- Ulises Vitulli <dererk@debian.org>  Thu, 08 Jan 2015 15:37:27 -0300

openntpd (20080406p-12) experimental; urgency=low

  * Fix sysv initscript race condition while restarting.
    Thanks AndrewAyer! (Closes: #773569).
  * Updated d/copyright to copyright-format 1.0.
  * Update Standards-Version to 3.9.6. No changes required.

 -- Ulises Vitulli <dererk@debian.org>  Mon, 29 Dec 2014 14:02:31 -0300

openntpd (20080406p-11) experimental; urgency=medium

  * Include apparmor profile and deps for mandatory access control.
  * Include systemd-timedated.service support: provide systemd ntp-units file.
    Thanks Martin-ÉricRacine! (Closes: #759671).

 -- Ulises Vitulli <dererk@debian.org>  Sat, 30 Aug 2014 00:02:22 -0300

openntpd (20080406p-10) unstable; urgency=medium

  * Fix glitch on systemd while entering on purging stage (Closes: #758399).

 -- Ulises Vitulli <dererk@debian.org>  Mon, 25 Aug 2014 19:47:11 -0300

openntpd (20080406p-9) unstable; urgency=high

  * Fix glitch on systemd starting multiple ntpd instances due to incorrect
    service file. Thanks ChristianBlichmann! (Closes: #758258).

 -- Ulises Vitulli <dererk@debian.org>  Tue, 19 Aug 2014 10:43:16 -0300

openntpd (20080406p-8) unstable; urgency=medium

  * The 'Whats-up-chief!' dupload.
  * Add systemd support (Closes: #756470):
    - debian/openntpd.service: new init start service, call dh_systemd_enable.
    - Added dh_systemd as build-dep.
  * Provide peer and sensors status to syslog through SIGUSR1:
    - 08-openntpd-signals-fix.patch: Thanks OsricWilkinson! (Closes: #753097).
    - 04-syslog.patch: Refreshed to reflect SIGUSR1 signal handling changes.

 -- Ulises Vitulli <dererk@debian.org>  Wed, 30 Jul 2014 11:07:32 -0300

openntpd (20080406p-7) unstable; urgency=medium

  * The 'Uhh-piece-of-candy!' dupload.
  * 06-pid.patch: re-introduce debian-specific patch for handling pidfiles
    (refreshed). Thanks SergeyBKirpichev for bringing it up! (Closes: #726650).
  * 07-dnstimeout.patch: handle DNS timeout lookups while querying for remote
    servers. Pulled from Gentoo, authored by PaulB.Henson (Thanks!).
  * Bumped up to Standards-Version 3.9.5.  No changes required.

 -- Ulises Vitulli <dererk@debian.org>  Mon, 16 Dec 2013 16:00:26 -0300

openntpd (20080406p-6) unstable; urgency=low

  * Remove driftfile at postrm when purging package (Closes: #702048).

 -- Ulises Vitulli <dererk@debian.org>  Sat, 02 Mar 2013 06:14:32 -0300

openntpd (20080406p-5) unstable; urgency=low

  * Create driftfile at postinst script, don't mess pkg's checksums, specially
    for security tools like debsums. Thanks Edi Meier (Closes: #701204).
  * Do not reload daemon when no network interface is declared, for example,
    if the user brings up any network interface manually.
    Thanks Valery Masiutsin (Closes: #701212).

 -- Ulises Vitulli <dererk@debian.org>  Fri, 22 Feb 2013 17:49:57 -0300

openntpd (20080406p-4) unstable; urgency=low

  * We now work on Arch: any, particularly kFreeBSD, switch from linux-any.

 -- Ulises Vitulli <dererk@debian.org>  Wed, 27 Jun 2012 19:22:37 -0300

openntpd (20080406p-3) unstable; urgency=high

  * Include local patch, fix kFreeBSD FTBFS due to openbsd-compat layer.
  * Refresh local patch, avoid noisy logging on ipv4-only systems
    (Closes: #673761).
  * Enhanced hardening flags and switched to dpkg-buildflags.
  * Cleanning and tidying up debian/rules.

 -- Ulises Vitulli <dererk@debian.org>  Wed, 27 Jun 2012 12:36:02 -0300

openntpd (20080406p-2) unstable; urgency=low

  * Fetched openntpd version from experimental to unstable:
    - Fixes tons of issues (check 20080406p-1 changelog for more information).
    - Drop local patches (mostly all of them upstream fixedi).
    - This is a upstream snapshot, please check out README.source.
  * Fix status target at initscript.

 -- Ulises Vitulli <dererk@debian.org>  Wed, 18 Apr 2012 12:46:42 -0300

openntpd (20080406p-1) experimental; urgency=low

  * New upstream snapshot from OpenNTPd Portable Branch porter, fix massively
    amount of reported and unreported bugs (Closes: #330587, #456664).
  * Upstream adopted ntp_adjtime approach, functionally identical to
    adjtimex, drops local patch on the way (Closes: #617801).
  * Sadly, we currently fail != linux because some portability issues related
    to ntpd_adjtime.
  * Implemented initscript configuration checks.
  * Updated debian/copyright to reflect recent changes.
  * Introduce ntpd.drift file:
    - Point --localstatedir to our path.
    - Updated manpage with proper paths on Debian.
    - Updated initscript for handling proper permissionships.
  * Changes on local patches:
    -Drop 02-reconnect.patch (Fixed on upstream).
    -Drop 04-backslash.patch (Fixed on upstream).
    -Drop 05-pid.patch (Removed).
    -Drop 06-servernameIfInvalid.patch (Fixed on upstream).
    -Drop 08-adjtimex.patch.patch (Fixed on upstream).
  * Bumped up to Standards-Version 3.9.3.  No changes required.
  * Report about some new features on debian/NEWS, implement it on the way.

 -- Ulises Vitulli <dererk@debian.org>  Thu, 08 Mar 2012 13:19:50 -0300

openntpd (3.9p1+debian-9) unstable; urgency=low

  * The 'room-for-one-more?--hell-yeeeeah!' dupload.
  * Runtime-depend on netbase to properly handle getservbyname() calls without
    hardcodes (Closes: #647152).
  * Bumped up to Standards-Version 3.9.2.0.  No changes required.
  * Improve building targets for simplifying portscripts.
  * Added collab-maint git repo at Vcs-Git and Vcs-Browser control fields.

 -- Ulises Vitulli <dererk@debian.org>  Tue, 06 Dec 2011 18:05:10 -0300

openntpd (3.9p1+debian-8) unstable; urgency=low

  * The 'Why-do-I-run-a-buildd-If-I-never-remember-to-use-it' dupload.
  * Use adjtimex() patch just under linux-based ports
    (Closes: #306106, #419219).

 -- Ulises Vitulli <dererk@debian.org>  Fri, 11 Feb 2011 07:37:21 -0300

openntpd (3.9p1+debian-7) unstable; urgency=low

  * The 'that-would-be-niceee' dupload.
  * Adopted portable openntpd's patch for adjtimex() to adjust kernel skew
    (Closes: #380737, #593429).
  * Specify which priority the events are sent to syslog (Closes: #502162).
  * Manpages clarification (Closes: #575705).

 -- Ulises Vitulli <dererk@debian.org>  Thu, 03 Feb 2011 18:16:42 -0300

openntpd (3.9p1+debian-6) unstable; urgency=low

  * The 'grrrrrrrrr' dupload.
  * debian/initscript:
   - Implement 'status' method.
   - Report about an already running process when trying to start.
   - When stopping, kill parent process, instead both (Closes: #599889)
     thanks Graham Wilson.
  * Bumped up to Standards-Version 3.9.1.  No changes required.

 -- Ulises Vitulli <dererk@debian.org>  Wed, 13 Oct 2010 10:27:27 -0300

openntpd (3.9p1+debian-5) unstable; urgency=low

  * Adding missing dependency on hardening-include.

 -- Ulises Vitulli <dererk@debian.org>  Wed, 09 Jun 2010 01:39:40 -0300

openntpd (3.9p1+debian-4) unstable; urgency=low

  * New maintainer (Closes: #543854).
  * The 'say-aaaaaaaaaaahh' upload.
  * Apply a pidfile patch, upstream doesn't like the idea of pidfiles, but
    sometimes they are useful for quick monitoring (Closes: #354825).
  * Avoid stripping by default on their own (Closes: #437691).
  * Dies if no peer servername is invalid, and logs badpeers through CRITICAL
    syslog. Thanks SergeyBKirpichev (Closes: #456661).
  * Depend on $network at init start and do not go through reloading if there
    is no listening interface declared. Thanks SergeyBKirpichev
    (Closes: #495528, #507586, #529984).
  * Explicity set default config file (Closes: #570253).
  * Enhance ntpd.conf manpage to verbosely remember we don't listen to any
    by default (Closes: #575705).
  * Switch to 3.0 source format, drop dependency on quilt.
  * Use hardening-include to handle gcc Hardening features.

 -- Ulises Vitulli <dererk@debian.org>  Tue, 08 Jun 2010 16:52:01 -0300

openntpd (3.9p1+debian-3) unstable; urgency=low

  * Updating package to standards version 3.8.3.
  * Removing vcs fields.
  * Orphaning package.

 -- Daniel Baumann <daniel@debian.org>  Thu, 27 Aug 2009 08:56:19 +0200

openntpd (3.9p1+debian-2) unstable; urgency=medium

  * Prefixing debhelper files with package name.
  * Using quilt rather than dpatch.
  * Applying patch from Stefan Praszalowicz
    <stefan.praszalowicz@avedya.com> to give a warning rather than fail on IPv4
    only networks when seeing an IPv6 DNS record (Closes: #500676).
  * Applying patch from Sergey B Kirpichev <skirpichev@gmail.com> to not fail if
    there is a backslash followed by a whitespaces in comments of the
    openntpd.conf (Closes: #435753).

 -- Daniel Baumann <daniel@debian.org>  Sat, 10 Jan 2009 23:06:29 +0100

openntpd (3.9p1+debian-1) unstable; urgency=low

  * Reverting config.guess and configu.sub to upstream.
  * Updating package to debhelper 6.
  * Adding homepage field in control file.
  * Adding vcs fields in control file.
  * Removing watch file.
  * Updatingto debhelper 7.
  * Updating to standards 3.8.0.
  * Updating vcs fields in control file.
  * Using patch-stamp rather than patch in rules file.
  * Adding symlink from ntpd manpage to openntpd (Closes: #501552).
  * Replacing obsolete dh_clean -k with dh_prep.
  * Merging upstream version 3.9p1+debian.
  * Reordering rules file.
  * Rewriting copyright file in machine-interpretable format.
  * Adding upstream target in rules file.

 -- Daniel Baumann <daniel@debian.org>  Sat, 08 Nov 2008 11:19:00 +0100

openntpd (3.9p1-7) unstable; urgency=low

  * Introducing /usr/sbin/openntpd symlink pointing and checking for that
    in the init script rather than for /etc/openntpd/ntpd.conf, thanks to
    Vincent Bernat <bernat@luffy.cx> (Closes: #423595).

 -- Daniel Baumann <daniel@debian.org>  Sun, 20 Jan 2008 12:02:00 +0100

openntpd (3.9p1-6) unstable; urgency=low

  * Added check for existence of /etc/openntpd/ntpd.conf in init script
    (Closes: #423595).
  * Including if-up.d script as suggested by Anarcat
    <anarcat@mumia.anarcat.ath.cx> to restart automatically when IP address
    changes (Closes: #457520).

 -- Daniel Baumann <daniel@debian.org>  Sun, 20 Jan 2008 11:04:00 +0100

openntpd (3.9p1-5) unstable; urgency=low

  * New maintainer (Closes: #459495).
  * Rediffed reconnect.dpatch and moved config patch from diff.gz to a dpatch.
  * Bumped package to new policy and debhelper 5.
  * Some formal cleanups in debian/*.
  * Conditionally call deluser in postrm.
  * Removed a bashism in postinst.
  * Updated watch file to version 3.
  * Applied patch from Petter Reinholdtsen <pere@hungry.com> to add LSB
    formatted dependency info to the init.d script (Closes: #460227).
  * Improved package description as suggested by Christine Spang
    <spang@mit.edu> and Kurt Roeckx <kurt@roeckx.be> (Closes: #403432).

 -- Daniel Baumann <daniel@debian.org>  Tue, 15 Jan 2008 18:11:00 +0100

openntpd (3.9p1-4) unstable; urgency=low

  * Reconnect after an EINVAL to make it work with dynamic IP
    addresses.  (Closes: #392250)

 -- Kurt Roeckx <kurt@roeckx.be>  Mon, 05 Mar 2007 18:40:48 +0100

openntpd (3.9p1-3) unstable; urgency=low

  * Add build dependency on groff.  Manpages where installed in
    the cat dir instead of the man dir.  (Closes: #378474)

 -- Kurt Roeckx <kurt@roeckx.be>  Sun, 16 Jul 2006 17:51:20 +0000

openntpd (3.9p1-2) unstable; urgency=low

  * Make sure that the /var/run/openntpd directory exists
    when starting the daemon (Closes: #374166)
  * Use --oknodo when stopping the daemon, so that it doesn't
    give an error when it's not running.
  * Change the default config to use the debian pool as time servers.

 -- Kurt Roeckx <kurt@roeckx.be>  Sun, 18 Jun 2006 13:25:02 +0000

openntpd (3.9p1-1) unstable; urgency=low

  * New upstream release
    - Remove getifaddrs_null.dpatch, merge upstream.
  * Remove the conflicts/replaces with xntp and xntp3.  They're not
    in the archive anymore for a long time.  (Closes: #316550)

 -- Kurt Roeckx <kurt@roeckx.be>  Sun, 11 Jun 2006 12:23:45 +0000

openntpd (3.7p1-1) unstable; urgency=low

  * New upstream release.  Removed the following patches:
    poll_errors.dpatch, freeifaddrs.dpatch, imsg_memmove.dpatch,
    network_unreachable.dpatch, daemonize_settime.dpatch.
    Keep: getifaddrs_null.dpatch
  * Remove the conflicts/replaces with xntp and xntp3.  They're not
    in the archive anymore for a long time.  (Closes: #316550)
  * Change Standards-Version to 3.6.2.  No changes required.

 -- Kurt Roeckx <kurt@roeckx.be>  Sat, 27 Aug 2005 17:36:57 +0200

openntpd (3.6.1p1-3) unstable; urgency=medium

  * getifaddrs_null.dpatch: getifaddrs() can return a interface doesn't have
    an address, like for instance on a ppp interface.  Skip the address
    instead of segfaulting.  Patch provided by Gabor Burjan
    <buga@buvoshetes.hu>.  (Closes: #310586)

 -- Kurt Roeckx <kurt@roeckx.be>  Tue, 24 May 2005 21:00:00 +0200

openntpd (3.6.1p1-2) unstable; urgency=medium

  * Change E-mail address to kurt@roeckx.be
  * poll_errors.dpatch: When poll() says there was an error on a socket, it
    didn't get checked and on the next poll() call got the same error again
    resulting in cpuhog. Patch from upstream CVS. (Closes: #307348)
  * Add a Depends on adduser since we use it in the postinst/postrm.
  * Use set -e in postinst/postrm instead of doing /bin/sh -e.
  * Create a defaults file, and mention the -s option in it.
  * freeifaddrs.dpatch: Call freeifaddrs() with the proper pointer.  Patch
    from upstream CVS.
  * imsg_memmove.dpatch: Use a memmove() instead of memcpy() as those buffers
    might overlap.  Patch from upstream CVS.
  * network_unreachable.dpatch: Do not consider ENETUNREACH as a fatal error.
    Patch from upstream CVS.
  * daemonize_settime.dpatch: When started with -s option (which isn't used by
    default) daemonize if sending failed instead of waiting for a reply.  This
    might be a problem when the network is down.  Patch from upstream CVS.
  * Remove the reload option from the init.d script since sending a SIGHUP
    does not work.  It just sets a flag and does nothing with that.

 -- Kurt Roeckx <kurt@roeckx.be>  Fri, 06 May 2005 15:04:54 +0200

openntpd (3.6.1p1-1) unstable; urgency=low

  * Initial Release. (Closes: #266022)

 -- Kurt Roeckx <Q@ping.be>  Wed, 18 Aug 2004 00:09:22 +0200
